﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson7
{
    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person
            {
                FullName = "Shadow Moon",
                Gender = "Male",
                Age = 55
            };

            using (var context = new DataContext())
            {
                context.People.Add(person);
                context.SaveChanges();

            }

        }

    }
}
